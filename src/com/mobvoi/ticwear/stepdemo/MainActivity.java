package com.mobvoi.ticwear.stepdemo;

import gl.android.utils.AndroidUtil;
import gl.java.tools.city.CityCodeUtil;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import com.way.weather.plugin.WeatherSpiderFactory;

@SuppressLint("SimpleDateFormat")
public class MainActivity extends Activity {
	final String TAG = "calandar";
	public final static String SP_KEY_CITYCODE = "citycode";
	@SuppressWarnings("unchecked")
	public <T> T find(int id) {
		return (T) findViewById(id);
	}

	@SuppressLint("SimpleDateFormat")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		initMornningAlarm();

		ListView listView = find(R.id.listView1);
		try {
			ArrayList<String> cityList = CityCodeUtil
					.readResouceFile(CityCodeUtil.filename);
			final ArrayList<HashMap<String, String>> data = new ArrayList<HashMap<String, String>>();
			final String key = "name";
			for (int i = 0; i < cityList.size(); i++) {
				final HashMap<String, String> hashMap = new HashMap<String, String>();
				hashMap.put(key, cityList.get(i));
				data.add(hashMap);
			}
			listView.setAdapter(new SimpleAdapter(this, data,
					android.R.layout.simple_list_item_1, new String[] { key },
					new int[] { android.R.id.text1 }));
			listView.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> parent, View view,
						final int position, long id) {
					new Thread(new Runnable() {

						@Override
						public void run() {
							String cityCode = data.get(position).get(key)
									.split(",")[0];
							AndroidUtil.saveToSharePreferences(SP_KEY_CITYCODE,"", cityCode);
							WeatherSpiderFactory factory = WeatherSpiderFactory
									.getInstance(getApplication(), cityCode);
							final WeatherSpiderFactory.ISpider spider = factory
									.getAvailableSpiders();
							spider.generateWeatherInfos();
							WeatherMessageNotification.notify(getApplication(),
									spider.getAlerts().toString()
											+ spider.getRealtime().toString(),
									position);
						}
					}).start();

				}
			});
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void initMornningAlarm() {
		final String value = "true";
		if (!value.equals(AndroidUtil
				.getValueFromSharePreferences("isFirst", ""))) {
			Intent intent = new Intent(getBaseContext(),
					MornningAlarmReceiver.class);
			intent.setAction("repeating");
			PendingIntent sender = PendingIntent.getBroadcast(getBaseContext(),
					0, intent, 0);

			Calendar calendar = Calendar.getInstance();
			calendar.setTimeInMillis(System.currentTimeMillis());
			calendar.add(Calendar.SECOND, 5);

			Log.i(TAG, new SimpleDateFormat("yyyy-MM-dd hh:mm:ss:SSSS")
					.format(calendar.getTime()));
			calendar.add(Calendar.SECOND, 5);
			calendar.set(Calendar.HOUR, 6);
			calendar.add(Calendar.DAY_OF_YEAR, 1);
			Log.i(TAG, new SimpleDateFormat("yyyy-MM-dd hh:mm:ss:SSSS")
					.format(calendar.getTime()));

			AlarmManager am = (AlarmManager) getSystemService(ALARM_SERVICE);
			am.cancel(sender);
			am.setRepeating(AlarmManager.RTC_WAKEUP,
					calendar.getTimeInMillis(), 24 *60*60* 1000, sender);
			AndroidUtil.saveToSharePreferences("isFirst", "", value);
		}

	}

}
