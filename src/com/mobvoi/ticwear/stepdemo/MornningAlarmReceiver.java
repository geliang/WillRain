package com.mobvoi.ticwear.stepdemo;

import gl.android.utils.AndroidUtil;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;

import com.way.weather.plugin.WeatherSpiderFactory;

public class MornningAlarmReceiver extends BroadcastReceiver {
	public MornningAlarmReceiver() {
	}

	@Override
	public void onReceive(final Context context, Intent intent) {
		// TODO: This method is called when the BroadcastReceiver is receiving
		// an Intent broadcast.
		System.out.println("MornningAlarmReceiver:" + intent);
		if (!TextUtils.isEmpty(AndroidUtil.getValueFromSharePreferences(
				MainActivity.SP_KEY_CITYCODE, ""))) {
			new Thread(new Runnable() {

				@Override
				public void run() {

					String cityCode = AndroidUtil.getValueFromSharePreferences(
							"SP_KEY_CITYCODE", "");
					WeatherSpiderFactory factory = WeatherSpiderFactory
							.getInstance(context, cityCode);
					final WeatherSpiderFactory.ISpider spider = factory
							.getAvailableSpiders();
					spider.generateWeatherInfos();
					WeatherMessageNotification.notify(context, spider
							.getAlerts().toString()
							+ spider.getRealtime().toString(), 0);
				}

			}).start();
		}
	}
}
