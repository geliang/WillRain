package gl.android.utils;

import net.tsz.afinal.FinalBitmap;
import android.app.Application;

public class App extends Application {
	private static App instance;
	public static FinalBitmap mFBitmap;
	public boolean isFirstUseApp = true;

	@Override
	public void onCreate() {
		super.onCreate();
		instance = this;
		// setup the hot patch
		// HotFixManager.getInstance().init(this);
		isFirstUseApp = AndroidUtil.isFirstUseApp();
		mFBitmap = FinalBitmap.create(this);
	}

	public static App getInstace() {
		return instance;

	}
}
