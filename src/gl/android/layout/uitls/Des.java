package gl.android.layout.uitls;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;

import android.annotation.SuppressLint;

public class Des {
	@SuppressLint("DefaultLocale")
	public static String enCrypto(String txt, String key)
			throws InvalidKeySpecException, InvalidKeyException,
			NoSuchPaddingException, IllegalBlockSizeException,
			BadPaddingException {
		StringBuffer sb = new StringBuffer();
		DESKeySpec desKeySpec = new DESKeySpec(key.getBytes());
		SecretKeyFactory skeyFactory = null;
		Cipher cipher = null;
		try {
			skeyFactory = SecretKeyFactory.getInstance("DES");
			cipher = Cipher.getInstance("DES");
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		SecretKey deskey = skeyFactory.generateSecret(desKeySpec);
		cipher.init(1, deskey);
		byte[] cipherText = (byte[]) null;
		try {
			cipherText = cipher.doFinal(txt.getBytes("UTF-8"));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		for (int n = 0; n < cipherText.length; n++) {
			String stmp = Integer.toHexString(cipherText[n] & 0xFF);

			if (stmp.length() == 1)
				sb.append("0" + stmp);
			else {
				sb.append(stmp);
			}
		}
		return sb.toString().toUpperCase();
	}

	public static String deCrypto(String txt) {
		try {
			return deCrypto(txt, "geliang0120");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return txt;
	}

	public static String enCrypto(String txt) {
		try {
			return enCrypto(txt, "geliang0120");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return txt;
	}

	public static String deCrypto(String txt, String key)
			throws InvalidKeyException, InvalidKeySpecException,
			NoSuchPaddingException, IllegalBlockSizeException,
			BadPaddingException {
		DESKeySpec desKeySpec = new DESKeySpec(key.getBytes());
		SecretKeyFactory skeyFactory = null;
		Cipher cipher = null;
		try {
			skeyFactory = SecretKeyFactory.getInstance("DES");
			cipher = Cipher.getInstance("DES");
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		SecretKey deskey = skeyFactory.generateSecret(desKeySpec);
		cipher.init(2, deskey);
		byte[] btxts = new byte[txt.length() / 2];
		int i = 0;
		for (int count = txt.length(); i < count; i += 2)
			btxts[(i / 2)] = (byte) Integer.parseInt(txt.substring(i, i + 2),
					16);
		try {
			return new String(cipher.doFinal(btxts), "UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return null;
	}
}