package gl.android.layout.uitls;

import android.util.Log;

public class Lg {
	public static void i(Object tag, Object msg) {
		Log.i(tag.getClass().getSimpleName(), msg.toString());
	}

	public static void e(Object tag, Object msg) {
		Log.e(tag.getClass().getSimpleName(), msg.toString());
	}
}
