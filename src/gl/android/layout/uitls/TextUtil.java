package gl.android.layout.uitls;

import gl.android.utils.App;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.BackgroundColorSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.UnderlineSpan;

public class TextUtil {
	public static SpannableString getHighLightSp(String s,
			String highLihgtString, int style, int highLihgtBgCloor) {
		SpannableString sp = new SpannableString(String.format(s,
				highLihgtString));

		int start = s.indexOf("%s");
		if (start >= 0) {
			// // 设置超链�?
			// sp.setSpan(new URLSpan("http://www.baidu.com"), 5, 7,
			// Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
			// 设置高亮样式�?
			int end = start + highLihgtString.length();
			sp.setSpan(new BackgroundColorSpan(highLihgtBgCloor), start, end,
					Spannable.SPAN_EXCLUSIVE_INCLUSIVE);
			sp.setSpan(
					new android.text.style.TextAppearanceSpan(App.getInstace(),
							style), start, end,
					Spannable.SPAN_EXCLUSIVE_INCLUSIVE);
			// // 设置高亮样式�?
			// sp.setSpan(new ForegroundColorSpan(Color.YELLOW), 20, 24,
			// Spannable.SPAN_EXCLUSIVE_INCLUSIVE);
		}

		return sp;
	}

	public static SpannableString getHighLightSp(String s, int startIndex,
			int endIndex, int style, int highLihgtBgCloor) {
		return getHighLightSp(new SpannableString(s), startIndex, endIndex,
				style, highLihgtBgCloor);
	}

	public static SpannableString getHighLightSp(SpannableString sp,
			int startIndex, int endIndex, int style, int highLihgtBgCloor) {
		int start = startIndex;
		// // 设置超链�?
		// sp.setSpan(new URLSpan("http://www.baidu.com"), 5, 7,
		// Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
		// 设置高亮样式�?
		int end = endIndex;
		sp.setSpan(new android.text.style.TextAppearanceSpan(App.getInstace(),
				style), start, end, Spannable.SPAN_EXCLUSIVE_INCLUSIVE);
		sp.setSpan(new BackgroundColorSpan(0x00000000), start, end,
				Spannable.SPAN_EXCLUSIVE_INCLUSIVE);
		sp.setSpan(new ForegroundColorSpan(highLihgtBgCloor), start, end,
				Spannable.SPAN_EXCLUSIVE_INCLUSIVE);

		sp.setSpan(new UnderlineSpan(), start, end,
				Spannable.SPAN_EXCLUSIVE_INCLUSIVE);
		// Blur a character
		// MaskFilterSpan mBlurMaskFilterSpan = new MaskFilterSpan(
		// new BlurMaskFilter(3, BlurMaskFilter.Blur.NORMAL));
		//
		// sp.setSpan(mBlurMaskFilterSpan, start, end,
		// Spannable.SPAN_EXCLUSIVE_INCLUSIVE);

		// MaskFilterSpan mMaskFilterSpan = new MaskFilterSpan(
		// new EmbossMaskFilter(new float[] { 1, 1, 1, 1 }, 0.4f, 6, 3.5f));
		// sp.setSpan(mMaskFilterSpan, start, end,
		// Spannable.SPAN_EXCLUSIVE_INCLUSIVE);

		// // 设置高亮样式�?
		// sp.setSpan(new ForegroundColorSpan(Color.YELLOW), 20, 24,
		// Spannable.SPAN_EXCLUSIVE_INCLUSIVE);

		return sp;
	}

	/**
	 * 对字符串中的数字和时间高亮处�?
	 * 
	 * @param s
	 * @return
	 */
	public static SpannableString getHighLightBlueString(String s) {
		ArrayList<int[]> list = findNumIndexList(s);
		SpannableString sp = new SpannableString(s);
		return sp;

	}

	/**
	 * 找个�?��字符串中�?��数字(0-9)的位�?
	 * 
	 * @param s
	 * @return �?��坐标 标记了每段数字的起始位置
	 */
	public static ArrayList<int[]> findNumIndexList(String s) {
		ArrayList<int[]> list = new ArrayList<int[]>();
		String str = s;

		int[] index = new int[] { -1, -1 };
		if (str != null && !"".equals(str)) {
			for (int i = 0; i < str.length(); i++) {
				if (i + 1 == str.length() && index[0] >= 0) {
					index[1] = i + 1;
					System.out.println("end:" + str.charAt(i));
					list.add(index);
					index = new int[] { -1, -1 };
					break;

				}
				if (str.charAt(i) >= 48 && str.charAt(i) <= 57) {
					if (i + 1 == str.length()) {
						if (index[0] < 0) {
							index[0] = i;
							System.out.println("start:" + str.charAt(i));
						}
						index[1] = i;
						System.out.println("end:" + str.charAt(i));
						list.add(index);
						index = new int[] { -1, -1 };
						break;
					} else if (index[0] < 0) {
						index[0] = i;
						System.out.println("start:" + str.charAt(i));
						continue;
					} else {
						continue;
					}
				} else {// 不是数字
					if (index[0] < 0) {
						continue;
					} else {
						if (i > 0) {
							System.out.println("end:" + str.charAt(i));
							index[1] = i;
							list.add(index);
							index = new int[] { -1, -1 };
						}
					}
				}
			}
		}
		for (int i = 0; i < list.size(); i++) {
			System.out.println(list.get(i)[0] + "," + list.get(i)[1]);
		}
		return list;
	}

	/**
	 * 
	 * 找一个字符串中一些时间字符串(YYY-MM-DD)的起始位�?
	 * 
	 * @param s
	 * @return �?��坐标 标记了每段提起的起始位置
	 */
	public static ArrayList<int[]> findTime(String string) {
		return findTime(string, "\\d{4}-\\d{2}-\\d{2}");
	}

	/**
	 * 
	 * 找一个字符串中一些时间字符串的起始位�?
	 * 
	 * @param s
	 * @param regExpress
	 *            for YYYY-MM-DD "\\d{4}-\\d{2}-\\d{2}" </br>for YYYY-MM-DD
	 *            HH:mm:ss "\\d{4}-\\d{2}-\\d{2} \\d{2}:\\d{2}:\\d{2}" </br>
	 *            eg:05 七月 2011 13:21:17,576 for �?�?�?小时：分钟：秒，毫秒 \d{2} \S+�?
	 *            \d{4} \d{2}:\d{2}:\d{2}\,\d{3}
	 * 
	 * @return �?��坐标 标记了每段提起的起始位置
	 */
	public static ArrayList<int[]> findTime(String string, String regExpress) {
		String regEx = regExpress;
		ArrayList<int[]> list = new ArrayList<int[]>();
		Pattern p = Pattern.compile(regEx);
		int[] index = new int[] { -1, -1 };
		String a1 = string;
		p = Pattern.compile(regEx);
		Matcher m = p.matcher(a1);
		while (m.find()) {
			String time = m.group();
			int offset = 0;
			if (list.size() > 0) {
				offset = list.get(list.size() - 1)[1];
			}
			index[0] = a1.indexOf(time, offset);
			index[1] = index[0] + time.length();
			list.add(index);
			index = new int[] { -1, -1 };
		}
		for (int i = 0; i < list.size(); i++) {
			System.out.println(list.get(i)[0] + "," + list.get(i)[1]);
		}
		return list;
	}
}
