package gl.android.layout.uitls;

import gl.android.utils.App;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.graphics.drawable.ColorDrawable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

public class Tip {

	@SuppressLint("InflateParams")
	public static void info(Context context, String msg) {
		LayoutInflater myInflater = LayoutInflater.from(context);
		View view = myInflater.inflate(android.R.layout.simple_list_item_1,
				null);

		TextView button = (TextView) view.findViewById(android.R.id.text1);
		button.setText(msg);
		button.setTextColor(0xf00);
		Toast mytoast = new Toast(context);
		mytoast.setGravity(Gravity.BOTTOM, 0, 128);
		mytoast.setView(view);
		mytoast.setDuration(Toast.LENGTH_SHORT);
		mytoast.show();
	}

	public static void show(Object message) {
		info(App.getInstace(), "" + message);
	}

	public static void show(int RstringID) {
		info(App.getInstace(), App.getInstace().getString(RstringID));
	}

	public static void show(View hostView, View contentView) {
		PopupWindow pop = new PopupWindow(contentView, hostView.getWidth(),
				LayoutParams.WRAP_CONTENT, true);
		pop.setBackgroundDrawable(new ColorDrawable(0x00000000));
		pop.showAsDropDown(hostView, 0, 0);
		pop.setOutsideTouchable(true);
	}

	public static void showTipDialog(Context content, String message,
			String title, DialogInterface.OnClickListener yesOnclickImp) {
		new AlertDialog.Builder(content).setMessage(message)
				.setPositiveButton("确定", yesOnclickImp).setTitle(title).show();

	}

	public static void showTipDialog(Context content, String message,
			String title, String postiveText,
			DialogInterface.OnClickListener yesOnclickImp) {
		new AlertDialog.Builder(content).setMessage(message)
				.setPositiveButton(postiveText, yesOnclickImp).setTitle(title)
				.show();

	}

	public static void showEnterDialog(Context content, String message,
			String title, DialogInterface.OnClickListener yesOnclickImp) {
		new AlertDialog.Builder(content).setMessage(message)
				.setPositiveButton("确定", yesOnclickImp)
				.setNeutralButton("否", new OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();

					}
				}).setTitle(title).show();

	}
}
