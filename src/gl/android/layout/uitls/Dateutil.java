package gl.android.layout.uitls;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class Dateutil {
	public final static SimpleDateFormat SimpleDateFormat__YYYYMMDDHHMMSSSS = new SimpleDateFormat(
			"yyyy-MM-dd  HH-mm-ss-SS");
	public final static SimpleDateFormat SimpleDateFormat__YYYYMMDD = new SimpleDateFormat(
			"yyyy-MM-dd");

	/**
	 * 默认返回当前时间�?年月日时分秒毫秒
	 * 
	 * @param sdf
	 * @return
	 */
	public static String getTimeString(SimpleDateFormat sdf, long mtime) {
		if (mtime <= 0) {
			mtime = System.currentTimeMillis();
		}
		if (sdf == null) {
			return SimpleDateFormat__YYYYMMDDHHMMSSSS.format(new Date(mtime));
		}
		return sdf.format(new Date(mtime));

	};

	public static String getNextDayString(int delay) {
		try {
			SimpleDateFormat format = new SimpleDateFormat("yyyy-M-dd");
			String mdate = "";
			Date d = new Date(System.currentTimeMillis());
			long myTime = (d.getTime() / 1000) + delay * 24 * 60 * 60;
			d.setTime(myTime * 1000);
			mdate = format.format(d);
			return mdate;
		} catch (Exception e) {
			return "";
		}
	}

	public static String milliSecendToHMSString(long milliSecend) {
		long totalSecond = milliSecend / 1000;
		long second = 0;
		long minute = 0;
		long hours = 0;
		long temp = totalSecond / 60;
		long temp2 = temp / 60;
		if (temp >= 60) {
			hours = temp2 % 60;
		}
		if (totalSecond >= 60) {
			minute = temp % 60;
		}
		second = totalSecond % 60;
		StringBuilder sb = new StringBuilder();
		sb.append(hours > 9 ? hours : "0" + hours);
		sb.append(":");
		sb.append(minute > 9 ? minute : "0" + minute);
		sb.append(":");
		sb.append(second > 9 ? second : "0" + second);
		return sb.toString();
	}

	public static long getDaysBetween(String fromString, String toString) {
		SimpleDateFormat df = new SimpleDateFormat("yyyy-M-dd");
		long to = 0l;
		long from = 0l;
		try {
			to = df.parse(fromString).getTime();
			from = df.parse(toString).getTime();
		} catch (ParseException e) {
			e.printStackTrace();
			return -1l;
		}
		return getDaysBetween(to, from);
	}

	public static boolean after(String dateString, int days) {
		String after30Day = getNextDayString(days);
		SimpleDateFormat df = new SimpleDateFormat("yyyy-M-dd");
		long from = 0l;
		try {
			from = df.parse(after30Day).getTime();
		} catch (ParseException e) {
			e.printStackTrace();
			return false;
		}

		Calendar tempCalendar = Calendar.getInstance();
		tempCalendar.setTime(new Date(from));

		Calendar tempCalendar2 = Calendar.getInstance();
		tempCalendar2.setTime(new Date(System.currentTimeMillis()));
		return tempCalendar2.after(tempCalendar);
	}

	/**
	 * 目标时间 是否�?当前时间�?+ days �?	 * 
	 * @param dateString
	 *            目标时间
	 * @param days
	 *            负数表示之前, 正数表示在这之后
	 * @return
	 */
	public static boolean before(String dateString, int days) {
		days = -1 * days;
		SimpleDateFormat df = new SimpleDateFormat("yyyy-M-dd");
		long from = 0l;
		long to = 0l;
		try {
			from = df.parse(dateString).getTime();

			String after30Day = getNextDayString(days);
			to = df.parse(after30Day).getTime();

			Calendar tempCalendar = Calendar.getInstance();
			tempCalendar.setTime(new Date(from));

			Calendar tempCalendar2 = Calendar.getInstance();
			tempCalendar2.setTime(new Date(to));
			System.out.println("DateUtil:before"
					+ (tempCalendar.get(Calendar.MONTH) + 1) + "-"
					+ tempCalendar.get(Calendar.DAY_OF_MONTH));
			System.out.println("DateUtil:before"
					+ (tempCalendar2.get(Calendar.MONTH) + 1) + "-"
					+ tempCalendar2.get(Calendar.DAY_OF_MONTH));
			return tempCalendar2.after(tempCalendar);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}

	}

	public static long getDaysBetween(long to, long from) {
		return (to - from) / (1000 * 60 * 60 * 24);
	}

	public static void main(String[] args) {
		System.out.println(before("2014-3-25", -15));
	}

	/**
	 * 返回当前的时�?	 * 
	 * @return default DataFormat "yyyy-MM-dd HH:mm:ss"
	 */
	public static String now() {
		return getTimeString(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"),
				System.currentTimeMillis());
	}

	/**
	 * �?��是否是同�?��
	 * 
	 * @param time
	 *            default DataFormat "yyyy-MM-dd HH:mm:ss"
	 * @return
	 */
	public static boolean IsSameDay(String time) {
		String timeNow = now();
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		boolean isSameDay = false;
		try {
			Calendar mCalendar1 = Calendar.getInstance();
			mCalendar1.setTime(df.parse(time));
			Calendar mCalendar2 = Calendar.getInstance();
			mCalendar2.setTime(df.parse(timeNow));
			if (mCalendar1.get(Calendar.DAY_OF_YEAR) == mCalendar2
					.get(Calendar.DAY_OF_YEAR)
					&& mCalendar1.get(Calendar.YEAR) == mCalendar2
							.get(Calendar.YEAR)) {
				isSameDay = true;
			}
		} catch (ParseException e1) {
			e1.printStackTrace();

		}
		if (time != null && time.length() > 0) {
			return isSameDay;
		}
		return false;
	}
}
