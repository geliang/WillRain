package gl.android.layout;

import android.text.TextUtils;

public class UserManger {
	public String token;
	public String username;
	public String password;
	public String expandname;
	public String expandurl;

	private UserManger() {
		System.out.println("init UserManger");
//		EventBus.getDefault().register(this);
	};

	protected void clear() {
//		EventBus.getDefault().unregister(this);
	}

	private static class SingletonHolder {
		private static final UserManger INSTANCE = new UserManger();
	}

	public static UserManger getInstance() {
		return SingletonHolder.INSTANCE;
	}

	public boolean checkToken() {
		if (TextUtils.isEmpty(token)) {
			return false;
		} else {
			return true;
		}

	}
}
