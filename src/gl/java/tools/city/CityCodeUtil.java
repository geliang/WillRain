package gl.java.tools.city;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class CityCodeUtil {
	private static ArrayList<String> cityTableCols;
	public final static String filename = "/CityCodeTable.db";

	public static ArrayList<String> readResouceFile(final String filename)
			throws FileNotFoundException, IOException {
		if (cityTableCols == null) {
			cityTableCols = new ArrayList<String>();
			BufferedReader br = new BufferedReader(new InputStreamReader(
					CityCodeUtil.class.getResourceAsStream(filename)));
			String readedLine = null;
			while ((readedLine = br.readLine()) != null) {
				if (readedLine.length()>0) {
					cityTableCols.add(readedLine);
				}
			}
			br.close();
		}
		return cityTableCols;
	}

	public static String getCityList(String cityName) {
		if (null==cityName||cityName.length()<=0) {
			return "";
		}
		try {
			ArrayList<String> cityTableCols = readResouceFile(filename);
			for (String cityInfo : cityTableCols) {
				final String[] split = cityInfo.split(",");
				if (split!=null&&split.length==4) {
					if (cityName.equals(split[1])) {
						return split[0];
					}
					
				}
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return "";
	}
	public static void main(String[] args) {
		System.out.println(getCityList("深圳"));
	}
}
